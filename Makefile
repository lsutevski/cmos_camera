.ONESHELL:
.PHONY: prepare run clean services

prepare:
	poetry install

run:
	poetry run python -m cam_control.main

clean:
	rm -r .venv poetry.lock


services: services/cam_control.service
	echo "> Creating links to services in /etc/systemd/system/"
	sudo ln -s `pwd`/services/*.service /etc/systemd/system/
	sudo systemctl daemon-reload

	echo "> Enabling services and starting server"
	sudo systemctl enable cam_control.service
	sudo systemctl start cam_control.service


services/cam_control.service:
	echo "> Creating services..."
	mkdir -p services
	cat <<EOF > $@
	[Unit]
	Description=A plugin to control the CMOS camera
	After=network.target
	
	[Service]
	User=$$USER
	WorkingDirectory=$$(pwd)
	ExecStart=$$(pwd)/.venv/bin/python -m cam_control.main
	Restart=always
	RestartSec=10
	
	[Install]
	WantedBy=multi-user.target
	EOF
