from base import MemoryDevice
import time
import json
import numpy as np
from base import RegisterBase

#-------------------------------------------------------------

class BIT:
    '''This class is used to address 8 bit registers, where only some bits need to be changed.
       Inputs are bits that need to be changed and values on that bits. The class generates 
       mask which can be applied to the current register in order to keep the current bits 
       unchanged. The two attributes available to the user are:
                bits.mask
                bits.val
    '''

    mask = ~np.uint8(0)
    val = np.uint8(0)
    '''This class is used to address 8 bit registers, where only some bits need to be addressed.'''
    def __init__(self, bit, val):
        if type(bit) == int:
            self.mask = self.mask & (~np.uint8(1<<bit))
            self.val = self.val | np.uint8(val<<bit)

        if type(bit) == type(slice(1,2)):
            start =  min(bit.indices(8)[0], bit.indices(8)[1])
            stop = max(bit.indices(8)[0], bit.indices(8)[1])
            for i in range(start, stop + 1):
                self.mask = self.mask & (~np.uint8(1<<i))

            self.val = self.val | np.uint8(val<<start)
                
        if type(bit) == list:
            for i,b in enumerate(bit):
                self.__init__(b, val[i])

#-------------------------------------------------------------

MIPI_DPHY_REGISTERS = {
    'CONTROL' : 0x0,
    'IDELAY_TAP_VALUE' : 0x4,
    'INIT' : 0x8,
    'CL_STATUS' : 0x18,
    'DL0_STATUS' : 0x1c,
    'DL1_STATUS' : 0x20,
    'DL2_STATUS' : 0x24,
    'DL3_STATUS' : 0x28,
    'HS_SETTLE_LANE1' : 0x30,
    'HS_SETTLE_LANE2' : 0x48,
}

class MIPI_DPHY(MemoryDevice):
    CONTROL : int = 0
    IDELAY_TAP_VALUE : int = 0
    INIT : int = 0
    CL_STATUS : int = 0
    DL0_STATUS : int = 0
    DL1_STATUS : int = 0
    DL2_STATUS : int = 0
    DL3_STATUS : int = 0
    HS_SETTLE_LANE1 : int = 0
    HS_SETTLE_LANE2 : int = 0


    _metadata = {
        'CONTROL' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'IDELAY_TAP_VALUE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'INIT' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'CL_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'DL0_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'DL1_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'DL2_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'DL3_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'DL3_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'HS_SETTLE_LANE1' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'HS_SETTLE_LANE2' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
    }

    def __init__(self, base_addr : int = 0x43c01000):
        self._initFlag = False
        super().__init__(base_addr)
        self._regs = MIPI_DPHY_REGISTERS
        self.Read()
        self._initFlag = True
        self._name = 'MIPI_DPHY'
    
    def Read(self):
        if self._initFlag:
            for key in self._regs:
                self.__dict__['__data_model__'][key] = self._read(self._regs[key])
        else:
            for key in self._regs:
                 self.__dict__[key] = self._read(self._regs[key])

    def Write(self):
        for reg in self._regs:
            self._write(self._regs[reg], self.__dict__[reg])

    def Save(self):
        '''Used to save the configuration of registers to a file registers.json
        '''
        dictionary = {key : self.__dict__['__data_model__'][key] for key in self._regs}
        json_object = json.dumps(dictionary, indent='\t')
        with open('{}.json'.format(self._name), 'w') as file:
            file.write(json_object)


#-------------------------------------------------------------

MIPI_RX_REGISTERS = {
    'CORE_CONFIG' : 0x0,
    'PROTOCOL_CONFIG' : 0x4,
    'CORE_STATUS' : 0x10,
    'GLOBAL_INTERRUPT_ENABLE' : 0x20,
    'INTERUPT_STATUS' : 0x24,
    'INTERUPT_ENABLE' : 0x28,
    'GENERIC_SHORT_PACKAGE' : 0x30,
    'VCX_FRAME_ERROR' : 0x34,
    'CLK_LANE' : 0x3C,
    'D0_LANE' : 0x40,
    'D1_LANE' : 0x44,
    'D2_LANE' : 0x48,
    'D3_LANE' : 0x4C,
    'IMAGE_INFO1_VC0' : 0x60,
    'IMAGE_INFO2_VC0' : 0x64,
}


class MIPI_RX(MemoryDevice):
    CORE_CONFIG : int = 0
    PROTOCOL_CONFIG : int = 0
    CORE_STATUS : int = 0
    GLOBAL_INTERRUPT_ENABLE : int = 0
    INTERUPT_STATUS : int = 0
    INTERUPT_ENABLE : int = 0
    GENERIC_SHORT_PACKAGE : int = 0
    VCX_FRAME_ERROR : int = 0
    CLK_LANE : int = 0
    D0_LANE : int = 0
    D1_LANE : int = 0
    D2_LANE : int = 0
    D3_LANE : int = 0
    IMAGE_INFO1_VC0 : int = 0
    IMAGE_INFO2_VC0 : int = 0


    _metadata = {
        'CORE_CONFIG' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'PROTOCOL_CONFIG' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'CORE_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'GLOBAL_INTERRUPT_ENABLE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'INTERUPT_STATUS' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'INTERUPT_ENABLE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'GENERIC_SHORT_PACKAGE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'VCX_FRAME_ERROR' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'CLK_LANE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'D0_LANE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'D1_LANE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'D2_LANE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'D3_LANE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'IMAGE_INFO1_VC0' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'IMAGE_INFO2_VC0' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
    }

    def __init__(self, base_addr : int = 0x43c00000):
        self._initFlag = False
        super().__init__(base_addr)
        self._regs = MIPI_RX_REGISTERS
        self.Read()
        self._initFlag = True
        self._name = 'MIPI_RX'
    
    def Read(self):
        if self._initFlag:
            for key in self._regs:
                self.__dict__['__data_model__'][key] = self._read(self._regs[key])
        else:
            for key in self._regs:
                 self.__dict__[key] = self._read(self._regs[key])

    def Write(self):
        for reg in self._regs:
            self._write(self._regs[reg], self.__dict__[reg])
    
    def Save(self):
        '''Used to save the configuration of registers to a file registers.json
        '''
        dictionary = {key : self.__dict__['__data_model__'][key] for key in self._regs}
        json_object = json.dumps(dictionary, indent='\t')
        with open('{}.json'.format(self._name), 'w') as file:
            file.write(json_object)

    
#-------------------------------------------------------------
    
VFB_REGISTERS = {
    'CONTROL' : 0x00,
    'GLOBAL_INTERRUPT_ENABLE' : 0x04,
    'INTERRUPT_ENABLE' : 0x08,
    'HEIGHT' : 0x18,
    'WIDTH' : 0x10,
    'STRIDE' : 0x20,
    'MEMORY_VIDEO_FORMAT' : 0x28,
    'PLANE_1_BUFFER' : 0x30
}

VFB_VALUES = {
    'CONTROL' : 0x01,
    'HEIGHT' : 750,
    'WIDTH' : 1280,
    'STRIDE' : 16384,
    'MEMORY_VIDEO_FORMAT' : 34,
    'PLANE_1_BUFFER' : 0x0f000000
}

class VFB(MemoryDevice):
    CONTROL : int = 0
    HEIGHT : int = 0
    WIDTH : int = 0
    STRIDE : int = 0
    MEMORY_VIDEO_FORMAT : int = 0
    PLANE_1_BUFFER : int = 0

    _metadata = {
        'CONTROL' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'HEIGHT' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'WIDTH' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'STRIDE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'MEMORY_VIDEO_FORMAT' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'PLANE_1_BUFFER' : {'min' : 0, 'max' : 0x1fffffff, 'step' : 1},
    }

    def __init__(self, base_addr : int = 0x43c10000):
        self._initFlag = False
        super().__init__(base_addr)
        self._regs = VFB_REGISTERS
        self._initialRegs = VFB_VALUES
        self._initialWrite()
        self.Read()
        self._initFlag = True
        self._name = 'Frame Buffer Write'
    
    def Read(self):
        if self._initFlag:
            for key in self._regs:
                self.__dict__['__data_model__'][key] = self._read(self._regs[key])
        else:
            for key in self._regs:
                 self.__dict__[key] = self._read(self._regs[key])

    def Write(self):
        for reg in self._regs:
            self._write(self._regs[reg], self.__dict__[reg])
    
    def _initialWrite(self):
        for reg in self._initialRegs:
            self._write(self._regs[reg], self._initialRegs[reg])
    
    def Save(self):
        '''Used to save the configuration of registers to a file registers.json
        '''
        dictionary = {key : self.__dict__['__data_model__'][key] for key in self._regs}
        json_object = json.dumps(dictionary, indent='\t')
        with open('{}.json'.format(self._name), 'w') as file:
            file.write(json_object)

    def LoadDefault(self):
        self._initialWrite()

#-------------------------------------------------------------

AXI_IIC_REGISTERS = {
    'GIE' : 0x01C,
    'ISR' : 0x020,
    'IER' : 0x028,
    'SOFTR' : 0x040,
    'CR' : 0x100,
    'SR' : 0x104,
    'TX_FIFO' : 0x108,
    'RX_FIFO' : 0x10C,
    'TX_FIFO_OCY' : 0x114,
    'RX_FIFO_OCY' : 0x118,
    'RX_FIFO_PIRQ' : 0x120,
}
class AXI_IIC(MemoryDevice):
    GIE : int = 0
    ISR : int = 0
    IER : int = 0
    SOFTR : int = 0
    CR : int = 0
    SR : int = 0
    TX_FIFO : int = 0
    RX_FIFO : int = 0
    TX_FIFO_OCY : int = 0
    RX_FIFO_OCY : int = 0
    RX_FIFO_PIRQ : int = 0

    _metadata = {
        'GIE' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'ISR' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'IER' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'SOFTR' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'CR' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'SR' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'TX_FIFO' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'RX_FIFO' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'TX_FIFO_OCY' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'RX_FIFO_OCY' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
        'RX_FIFO_PIRQ' : {'min' : 0, 'max' : 0xffff, 'step' : 1},
    }

    def __init__(self, base_addr : int = 0x43c01000):
        self._initFlag = False
        super().__init__(base_addr)
        self._regs = AXI_IIC_REGISTERS
        self.Read()
        self._initFlag = True
        self._name = 'AXI IIC Registers'
    
    def Read(self):
        if self._initFlag:
            for key in self._regs:
                self.__dict__['__data_model__'][key] = self._read(self._regs[key])
        else:
            for key in self._regs:
                 self.__dict__[key] = self._read(self._regs[key])
    
    def _Write(self, reg, val):
        if type(val) == BIT:
            val = (self._read(reg) & val.mask) | val.val
        self._write(reg, int(val))
        
    def _writeBits(self, reg, val : BIT):
        val = (self._read(reg) & val.mask) | val.val
        self._write(reg, val)


ADV7511_REGISTER_ADDRESSES = {
    'REG_0x41' : 0x41,
    'REG_0x98' : 0x98,
    'REG_0x9A' : 0x9A,
    'REG_0x9C' : 0x9C,
    'REG_0x9D' : 0x9D,
    'REG_0xA2' : 0xA2,
    'REG_0xA3' : 0xA3,
    'REG_0xE0' : 0xE0,
    'REG_0xF9' : 0xF9,
    #Video input and output formats
    'REG_0x15' : 0x15,
    'REG_0x16' : 0x16,
    'REG_0x17' : 0x17,
    'REG_0x18' : 0x18,
    'REG_0xAF' : 0xAF,
    'REG_0x40' : 0x40,
    'REG_0x4C' : 0x4C,
    #Hot plug detect and monitor sense
    'REG_0x96' : 0x96,
    'REG_0x42' : 0x42,
    #Some random pull down resistor
    'REG_0xE0' : 0xE0,
    'REG_0x55' : 0x55,
    #Regs for configuring image format
    'REG_0x9D' : 0x9D,
    'REG_0xA4' : 0xA4,
    'REG_0xBA' : 0xBA,
    'REG_0xD0' : 0xD0,
}

ADV7511_INITIAL_VALS = {
    'REG_0x98' : 0x03,
    'REG_0x9A' : 0xE0,
    #'REG_0x9A' : BIT(slice(7,5), 0b111),
    'REG_0x9C' : 0x30,
    #'REG_0x9D' : BIT(slice(1,0), 0b01),
    'REG_0x9D' : 0x61,
    'REG_0xA2' : 0xA4,
    'REG_0xA3' : 0xA4,
    'REG_0xE0' : 0xD0,
    'REG_0xF9' : 0x00,
    'REG_0x55' : 0x02,
    #configure color and frame format -> 8 bit yuv 4:2:2
    'REG_0x15' : 0x01,
    'REG_0x16' : 0b10111100,
    'REG_0x9D' : 0x00,
    'REG_0xA4' : 0x00
}

class ADV7511_REGISTERS(RegisterBase):
    REG_0x41: int
    REG_0x98: int
    REG_0x9A: int
    REG_0x9C: int
    REG_0x9D: int
    REG_0xA2: int
    REG_0xA3: int
    REG_0xE0: int
    REG_0xF9: int
    REG_0x15: int
    REG_0x16: int
    REG_0x17: int
    REG_0x18: int
    REG_0xAF: int
    REG_0x40: int
    REG_0x4C: int

    _metadata = {
        'REG_0x41' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x98' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x9A' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x9C' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x9D' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0xA2' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0xA3' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0xE0' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0xF9' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x15' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x16' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x17' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x18' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0xAF' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x40' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REG_0x4C' : {'min' : 0, 'max' : 0xff, 'step' : 1},
    }

class ADV7511_CONTROL:
    def __init__(self, dev_addr: int = 0x72, base_addr : int = 0x41600000):
        self._initFlag = False

        self._regAddr = ADV7511_REGISTER_ADDRESSES
        self.regs = ADV7511_REGISTERS()
        self.AXI_IIC = AXI_IIC(base_addr)

        self._dev_addr = dev_addr

        #initialisation
        self.AXI_IIC._write(self.AXI_IIC._regs['SOFTR'], 0xa)
        self.AXI_IIC._write(self.AXI_IIC._regs['RX_FIFO_PIRQ'], 0x0f)

        self.Read()

        self._initFlag = True
    
    def _read(self, reg):
        #initialisation
        self.AXI_IIC._write(self.AXI_IIC._regs['SOFTR'], 0xa)
        self.AXI_IIC._write(self.AXI_IIC._regs['RX_FIFO_PIRQ'], 0x0f)

        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(1, 0b1))
        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(0, 0b1))
        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(1, 0b0))

        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(6, 0b0))

        SR = self.AXI_IIC._read(self.AXI_IIC._regs['SR'])

        timeout = 1
        start = time.time()
        timedOut = True

        #first we want to write a register address
        self.AXI_IIC._write(self.AXI_IIC._regs['TX_FIFO'], self._dev_addr | 1<<8)
        self.AXI_IIC._write(self.AXI_IIC._regs['TX_FIFO'], reg)

        #then lets go for reading from the device
        self.AXI_IIC._write(self.AXI_IIC._regs['TX_FIFO'], self._dev_addr | 1<<8 | 1)
        self.AXI_IIC._write(self.AXI_IIC._regs['TX_FIFO'], 1 | 1<<9)

        while time.time() < start + timeout:
            SR = np.uint32(self.AXI_IIC._read(self.AXI_IIC._regs['SR']))
            RX_FIFO_EMPTY = (SR >> 6) & np.uint32(1)

            if RX_FIFO_EMPTY != 1:
                timedOut = False
                break

        if timedOut:
            raise RuntimeWarning("Request timed out. RX FIFO is empty.")
        else:
            return np.uint8(self.AXI_IIC._read(self.AXI_IIC._regs['RX_FIFO']))
    
    def _write(self, reg, val):
        #initialisation
        self.AXI_IIC._write(self.AXI_IIC._regs['SOFTR'], 0xa)
        self.AXI_IIC._write(self.AXI_IIC._regs['RX_FIFO_PIRQ'], 0x0f)

        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(1, 0b1))
        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(0, 0b1))
        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(1, 0b0))

        self.AXI_IIC._writeBits(self.AXI_IIC._regs['CR'], BIT(6, 0b0))

        if type(val)==BIT:
            val = (self._read(reg) & val.mask) | val.val

        SR = np.uint32(self.AXI_IIC._read(self.AXI_IIC._regs['SR']))
        RX_FIFO_EMPTY = (SR >> 6) & np.uint32(1)
        TX_FIFO_EMPTY = (SR >> 7) & np.uint32(1)
        BB = (SR >> 2) & np.uint32(1)

        if RX_FIFO_EMPTY == 1 and TX_FIFO_EMPTY == 1 and BB == 0:
            self.AXI_IIC._write(self.AXI_IIC._regs['TX_FIFO'], self._dev_addr | 1<<8)
            self.AXI_IIC._write(self.AXI_IIC._regs['TX_FIFO'], np.uint32(reg))
            self.AXI_IIC._write(self.AXI_IIC._regs['TX_FIFO'], np.uint32(val) | np.uint32(1)<<9)

        else:
            raise RuntimeWarning("Transaction could not be done because either FIFO's are not empty or Bus is busy.")

    def Read(self):
        if self._initFlag:
            for key in self._regAddr:
                val = self._read(self._regAddr[key])
                print(val)
                self.regs.__dict__['__data_model__'][key] = int(val)
        else:
            for key in self._regAddr:
                val = self._read(self._regAddr[key])
                print(val)
                self.regs.__dict__[key] = int(val)
    
    def Set(self):
        for key in self._regAddr:
            self._write(self._regAddr[key], self.regs.__dict__[key])
    
    def setInitial(self):
        for key in ADV7511_INITIAL_VALS:
            self._write(self._regAddr[key], ADV7511_INITIAL_VALS[key])

    def powerDown(self):
        self._write(0x41 , 0x00)
#-------------------------------------------------------------

TPG_REGISTERS = {
    'CONTROL' : 0x00,
    'GLOBAL_INTERRUPT_ENABLE' : 0x04,
    'INTERRUPT_ENABLE' : 0x08,
    'INTERRUPT_STATUS' : 0x0C,
    'HEIGHT' : 0x10,
    'WIDTH' : 0x18,
    'BACKGROUND_PATTERN_ID' : 0x20,
    'COLOR_FORMAT' : 0x40,
}

TPG_VALUES = {
    'HEIGHT' : 720,
    'WIDTH' : 1280,
    'BACKGROUND_PATTERN_ID' : 0x01,
    'COLOR_FORMAT' : 0x2,
}

class TPG(MemoryDevice):
    CONTROL : int = 0
    GLOBAL_INTERRUPT_ENABLE : int = 0
    INTERRUPT_ENABLE : int = 0
    INTERRUPT_STATUS : int = 0
    HEIGHT : int = 0
    WIDTH : int = 0
    BACKGROUND_PATTERN_ID : int = 0
    COLOR_FORMAT : int = 0

    def __init__(self, base_addr : int = 0x43c00000):
        print('started')
        super().__init__(base_addr)
        self._regs = TPG_REGISTERS
        self._initialRegs = TPG_VALUES
        print("writting initial vals")
        self._initialWrite()
        self.Read()

    def Read(self):
        for key in self._regs:
            self.__dict__[key] = self._read(self._regs[key])
            print('{} : {}'.format(key, self.__dict__[key]))

    def Write(self):
        for reg in self._regs:
            self._write(self._regs[reg], self.__dict__[reg])

    def _initialWrite(self):
        for reg in self._initialRegs:
            self._write(self._regs[reg], self._initialRegs[reg])