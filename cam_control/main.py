#!/usr/bin/env python
# -*- coding:utf-8 -*-
#
# Created: 10/2022
# Author: Leo Sutevski <lsutevski@ethz.ch>

'''
Module docstring
'''
import base64
import numpy as np
import os
from tracemalloc import start
import tiqi_plugin
from tiqi_plugin.decorators import Saver
#from dummy_backend import Camera
import asyncio
import warnings
import json
import time
import threading
import logging
from CMOS_backend import Camera
from ControlPins import ControlPin
from XilinxIP_backend import MIPI_DPHY, MIPI_RX, VFB, ADV7511_CONTROL, TPG
from MemoryBackend import MemoryFrame
from Pin import Pin, SYSFS_BASE_PATH, SYSFS_GPIO_PATH
from time import gmtime, strftime

class DataLog:
    '''Simple class of data logger'''
    def __init__(self, name = 'LOG_'+time.strftime("%H_%M_%S-%d_%m") + ".txt", name_row = []):
        with open(name, "w") as f:
            line = ""
            for col in name_row:
                if line == "":
                    line = line + col
                else:
                    line = line + "," + col
            f.write(line)
        self.name = name
        self.name_row = name_row
    
    def writeline(self, line : dict):
        line_str = ""
        with open(self.name, "a") as f:
            f.write('\n')
            for col in self.name_row:
                if type(line[col]) == int:
                    val = hex(line[col])
                else:
                    val = str(line[col])
                if line_str == "":
                    try:
                        line_str = line_str + str(val)
                    except KeyError:
                        line_str = line_str + 'NaN'
                else:
                    try:
                        line_str = line_str + ", " + str(val)
                    except KeyError:
                        line_str = line_str + "," + 'NaN'
            f.write(line_str)

def BITS(num : int, start, stop = 0, flag = False):
    '''This function is a helper function for extracting certain bits of num.
       Example: num = 13 = 0b1101
                BITS(num, start = 2, stop = 0) = 0b101 =  5
    '''
    if not flag:
        l = 34
        num_str = '0b' + '{:#034b}'.format(num)[l-start-1:l-stop-1] + '{:#034b}'.format(num)[l-stop-1]
    else:
        num_str = '0b'
        for i in range(start+1):
            if i <= (start-stop):
                num_str = num_str + '1'
            else:
                num_str = num_str + '0'
    
    return int(num_str, base = 2)
        

WEBPORT = 8000
RPCPORT = 6001
class FancyControl:
    def __init__(self, camera : Camera, EnablePin : ControlPin, AXIreset : ControlPin,
                 mipi_dphy : MIPI_DPHY, mipi_rx: MIPI_RX, control):
        self._camera = camera
        self._enable = EnablePin
        self._mipi_dphy = mipi_dphy
        self._mipi_rx = mipi_rx
        self._areset = AXIreset
        self._control = control
        self._name = "Control"
        

        self._calibration_running = False

    def Start(self):
        self._camera.start()

    def Stop(self):
        self._camera.stop()
    
    def Reset(self):
        self._enable._pin.reset()
        self._areset._pin.reset()
        time.sleep(1)
        self._enable._pin.set()
        self._areset._pin.set()
        self._camera.reset()
        self._control.VFB._initialWrite()

    def _startlogging(self):
        name_row = ['idelay0', 'idelay1', 'SoT_Error', 'ECC_2bit', 'ECC_1bit', 
                    'INTERUPT_STATUS', 'GENERIC_SHORT_PACKAGE', 'D0_LANE', 
                    'D1_LANE', 'DL0_STATUS', 'DL1_STATUS', 'IDELAY_TAP_VALUE', 'CORE_STATUS', 'VCX_FRAME_ERROR']
        self._logger = DataLog(name = 'CALIBRATION_' + time.strftime("%H_%M_%S-%d_%m") + ".txt", name_row = name_row)
    
    def _logvalues(self, idelay0, idelay1):
        values = {
            'idelay0' : idelay0,
            'idelay1' : idelay1,
            'DL0_STATUS' : self._mipi_dphy._read(self._mipi_dphy._regs['DL0_STATUS']),
            'DL1_STATUS' : self._mipi_dphy._read(self._mipi_dphy._regs['DL1_STATUS']),
            'D0_LANE' : self._mipi_rx._read(self._mipi_rx._regs['D0_LANE']),
            'D1_LANE' : self._mipi_rx._read(self._mipi_rx._regs['D1_LANE']),
            'GENERIC_SHORT_PACKAGE' : self._mipi_rx._read(self._mipi_rx._regs['GENERIC_SHORT_PACKAGE']),
            'INTERUPT_STATUS' : self._mipi_rx._read(self._mipi_rx._regs['INTERUPT_STATUS']),
            'IDELAY_TAP_VALUE' : self._mipi_dphy._read(self._mipi_dphy._regs['IDELAY_TAP_VALUE']),
            'CORE_STATUS' : self._mipi_rx._read(self._mipi_rx._regs['CORE_STATUS']),
            'VCX_FRAME_ERROR' : self._mipi_rx._read(self._mipi_rx._regs['VCX_FRAME_ERROR']),
        }
        interrupt = values['INTERUPT_STATUS']
        values['SoT_Error'] = str(BITS(interrupt, 13, 13))
        values['ECC_2bit'] = str(BITS(interrupt, 11, 11))
        values['ECC_1bit'] = str(BITS(interrupt, 10, 10))

        self._logger.writeline(values)

    def _Calibrate_IDELAYs(self):
        self._startlogging()
        IDELAY_values = list(np.arange(1,32))
        self._mipi_rx.Read()
        self._mipi_dphy.Read()
        settle_time = self._mipi_dphy._read(self._mipi_dphy._regs['HS_SETTLE_LANE1'])
        active_lanes = BITS(self._mipi_rx.PROTOCOL_CONFIG , 1, 0)
        logging.warning('Thread process started')
        if active_lanes == 1:
            for delay0 in IDELAY_values:
                if not self._calibration_running:
                    break
                for delay1 in IDELAY_values:
                    if not self._calibration_running:
                        break
                    self.Stop()
                    self.Reset()
                    time.sleep(0.5)
                    idelayreg = self._mipi_dphy._read(self._mipi_dphy._regs['IDELAY_TAP_VALUE'])
                    regval = ((delay0<<0) & BITS(0, 4, 0, True)) | ((delay1<<8) & BITS(0, 12, 8, True)) | (idelayreg&(BITS(0, 12, 8, True)^0xffffffff)) | (idelayreg&(BITS(0, 12, 8, True)^0xffffffff))
                    print(hex(regval))
                    self._mipi_dphy._write(self._mipi_dphy._regs['IDELAY_TAP_VALUE'], regval)
                    self._control.Load()
                    self._control.Set()
                    print(self._mipi_dphy._read(self._mipi_dphy._regs['IDELAY_TAP_VALUE']))
                    self._mipi_dphy._write(self._mipi_dphy._regs['HS_SETTLE_LANE1'], settle_time)
                    self._mipi_dphy._write(self._mipi_dphy._regs['HS_SETTLE_LANE2'], settle_time)
                    time.sleep(0.5)
                    self.Start()
                    time.sleep(2)
                    self._mipi_dphy.Read()
                    self._mipi_rx.Read()
                    self._logvalues(delay0, delay1)
        elif active_lanes == 3:
            for delay0 in IDELAY_values:
                for delay1 in IDELAY_values:
                    for delay2 in IDELAY_values:
                        for delay3 in IDELAY_values:
                            pass
        
        self.Calibrating = False
        logging.warning('Thread process finished')
    
    @property
    def Calibrating(self):
        '''Run the camera'''
        return self._calibration_running
    
    @Calibrating.setter
    def Calibrating(self, value):
        self._calibration_running = value
        if value:
            process = threading.Thread(target=self._Calibrate_IDELAYs, daemon=True)
            process.start()

class Operation:
    _gain: int = 0
    _blklevel: int = 0
    _integrationtime: int = 0

    _metadata = {
        'Gain' : {'min' : 0, 'max' : 240, 'units': '0.3 dB', 'renderAs' : 'slider'},
        'BlkLevel' : {'min' : 0, 'max' : 255, 'renderAs' : 'slider'},
        'IntegrationTime' : {'min' : 1, 'max' : 0x3FFF-2, 'units': 'H', 'renderAs': 'slider'}
    }
    def __init__(self, cam: Camera):
        self._cam = cam
        
        self._updateParams()
        
    def _read3partReg(self, reg: str):
        MSB = self._cam._read(self._cam.REGISTER_ADDRESSES[reg + '_MSB'])
        SB = self._cam._read(self._cam.REGISTER_ADDRESSES[reg + '_SB'])
        LSB = self._cam._read(self._cam.REGISTER_ADDRESSES[reg + '_LSB'])

        val = MSB << 16 | SB << 8 | LSB

        print(reg + '|MSB, SB, LSB: {}, {}, {} | val = {}'.format(MSB, SB, LSB, val))
        return  val
    
    def _write3partReg(self, reg, val):
        MSB_mask = np.uint32(0x00FF0000)
        SB_mask = np.uint32(0x0000FF00)
        LSB_mask = np.uint32(0x000000FF)
        
        val = np.uint32(val)

        MSB_val = np.uint8((MSB_mask & val) >> 16)
        SB_val = np.uint8((SB_mask & val) >> 8)
        LSB_val = np.uint8(LSB_mask & val)

        self._cam._write(self._cam.REGISTER_ADDRESSES[reg + '_LSB'], LSB_val)
        self._cam._write(self._cam.REGISTER_ADDRESSES[reg + '_SB'], SB_val)
        self._cam._write(self._cam.REGISTER_ADDRESSES[reg + '_MSB'], MSB_val)

    def _updateParams(self):
        self._VMAX = self._read3partReg('VMAX')
        self.Gain = self._cam._read(self._cam.REGISTER_ADDRESSES['GAIN'])
        self.IntegrationTime = self._VMAX - self._read3partReg('SHS1') - 1
        self.BlkLevel = self._cam._read(self._cam.REGISTER_ADDRESSES['BLKLEVEL_LSB'])
    
    @property
    def Gain(self) -> int:
        return self._gain
    
    @Gain.setter
    def Gain(self, value: int):
        self._cam._write(self._cam.REGISTER_ADDRESSES['GAIN'], value)
        self._gain = value
    
    @property
    def BlkLevel(self) -> int:
        return self._blklevel
    
    @BlkLevel.setter
    def BlkLevel(self, value: int):
        self._cam._write(self._cam.REGISTER_ADDRESSES['BLKLEVEL_LSB'], value)
        self._blklevel = value
    
    @property
    def IntegrationTime(self) -> int:
        return self._integrationtime
    
    @IntegrationTime.setter
    def IntegrationTime(self, value: int):
        self._write3partReg('SHS1', self._VMAX - 1 - value)
        print(self._VMAX - 1 - value)
        self._integrationtime = value

    def Initialise(self):
        self._cam.reset()
        self._updateParams()
    
    def SetDefault(self):
        self._cam.writeDefault()
        self._updateParams()

    def Start(self):
        self._cam.start()

    def Stop(self):
        self._cam.stop()

class ControlIDELAY_2lanes:
    Lane0Delay : int
    Lane1Delay : int
    _metadata = {
        'Lane0Delay' : {'min' : 0, 'max' : 31},
        'Lane1Delay' : {'min' : 0, 'max' : 31}
    }

    def __init__(self):
        self._lane0delay = 1
        self._lane1delay = 1

        self._loadPin = ControlPin(pin_number = 971)
        self.resetPin = ControlPin(pin_number = 970)

        self.lane0_pins = [
            ControlPin(pin_number = 960),
            ControlPin(pin_number = 961),
            ControlPin(pin_number = 962),
            ControlPin(pin_number = 963),
            ControlPin(pin_number = 964),
        ]
        self.lane1_pins = [
            ControlPin(pin_number = 965),
            ControlPin(pin_number = 966),
            ControlPin(pin_number = 967),
            ControlPin(pin_number = 968),
            ControlPin(pin_number = 969),
        ]

        self._setPins()

    
    def _setPins(self):
        lane = "{:05b}".format(self._lane0delay)
        l = len(lane)
        for i, pin in enumerate(self.lane0_pins):
            pin.Value = int(lane[l-1-i])

        lane = "{:05b}".format(self._lane1delay)
        l = len(lane)
        for i, pin in enumerate(self.lane1_pins):
            pin.Value = int(lane[l-1-i])
        
        self._loadPin.Value = 1
        time.sleep(0.01)
        self._loadPin.Value = 0

    @property
    def Lane0Delay(self) -> int:
        return self._lane0delay

    @Lane0Delay.setter
    def Lane0Delay(self, value):
        self._lane0delay = value
        self._setPins()
    
    @property
    def Lane1Delay(self) -> int:
        return self._lane1delay

    @Lane1Delay.setter
    def Lane1Delay(self, value):
        self._lane1delay = value
        self._setPins()

class SaveImages:
    def __init__(self, control) -> None:
        self._saving = False
        self._current_frame = None
        self._chunksize = 5
        self._metadata = {
            'ChunkSize' : {'min' :  1, 'max' : 20}
        }
        self._cntr = 1
        tmp_path = "/tmp"
        self._control = control
        self._tmp_folder = os.path.join(tmp_path,"{}".format(strftime("%a_%d_%b_%Y", gmtime())))
        if not os.path.isdir(self._tmp_folder):
            os.mkdir(self._tmp_folder)

    def Save(self, name):
        np.save(os.path.join(self._tmp_folder, "im_{}.npy".format(name)), self._tmp_folder)

    @property
    def Saving(self):
        return self._saving
    
    @Saving.setter
    def Saving(self, val):
        self._saving = val
    
    @property
    def ChunkSize(self):
        return self._chunksize
    
    @ChunkSize.setter
    def ChunkSize(self, val):
        self._chunksize = val


class CamControl:
    _initflag = False
    _metadata = {
        'Image' : {'renderAs': 'image'}
    }
    
    def __init__(self) -> None:
        self._flag = False
        self._EnablePin = ControlPin(pin_number=990)
        self._AXIreset = ControlPin(pin_number=991)  #active LOW
        self.ControlPins = [self._EnablePin, self._AXIreset]

        self.HDMIPin = ControlPin(pin_number=960)

        self.MIPI_DPHY = MIPI_DPHY()
        #self.TPG = TPG()
        self.MIPI_RX = MIPI_RX()

        self.VFB = VFB()
        self.Frame = MemoryFrame()
        self._cam = Camera()
        self.regs = self._cam.REGISTERS
        self.Read()
        self.Random_regs = self._cam.RandomRegister

        self.Operation = Operation(self._cam)
        #self.DelayControl = ControlIDELAY_2lanes()

        #self.Fancy = FancyControl(self._cam, self._EnablePin, self._AXIreset, 
        #                          self.MIPI_DPHY, self.MIPI_RX, self)
        
        #self.ADV7511 = ADV7511_CONTROL()

        self._running: bool = False
        self._streaming: bool = False

        self._hexadecimal: str = '0x00'
        self._decimal: int = 0
        self._initflag = True
        with open("./cam_control/ions1.png", "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read()).decode('utf-8')

        self.Saving = SaveImages(self)
        self.Image = encoded_string

        #SYSFS_EXPORT_PATH   = SYSFS_BASE_PATH + '/export'
        #if not os.path.isdir(SYSFS_GPIO_PATH % 960):
        #    with open(SYSFS_EXPORT_PATH, 'w') as export:
        #        export.write('%d' % 960)
        #self._interruptPin = Pin(number=960, direction='in', edge = 'rising',
        #                        callback = self._callback)
    
    def _callback(self, *args):
        print("Callback called. Arguments are: {}".format(args))

    @property
    def Hexadecimal(self):
        return self._hexadecimal
    
    @Hexadecimal.setter
    def Hexadecimal(self, value):
        if '0x' not in value[:2]:
            pass
        else:
            if not self._flag:
                self._hexadecimal = value
                self._flag = True
                self.Decimal = int(value, base = 16)
            
            else:
                self._flag = False
                self._hexadecimal = value
    
    @property
    def Decimal(self):
        return self._decimal
    
    @Decimal.setter
    def Decimal(self, value):
        if not self._flag:
            self._decimal = value
            self._flag = True
            self.Hexadecimal = hex(value)
        
        else:
            self._flag = False
            self._decimal = value
    
    @property
    def Running(self):
        '''Run the camera'''
        return self._running
    
    @Running.setter
    def Running(self, value):
        self._running = value
        if value:
            #loop = asyncio.new_event_loop()
            #loop.run_until_complete(self._start_rotation())
            #loop.create_task(self._start_rotation())
            process = threading.Thread(target=self._rotation, daemon=True)
            process.start()

    @property
    def Streaming(self):
        '''Run the camera'''
        return self._streaming
    
    @Streaming.setter
    def Streaming(self, value):
        self._streaming = value
        if value:
            process = threading.Thread(target=self._startStreaming, daemon=True)
            process.start()
        
    def Set(self):
        self._cam.configure(**{key : self.regs.__dict__[key] for key in self._cam.REGISTER_ADDRESSES})
        
    
    def Write_Random(self):
        #self._cam._write(self.Random_regs._random_register, self.Random_regs._value)
        pass
    
    def Read(self):
        '''This is just a test
        '''
        dictionary = self._cam.read_config(self._cam.REGISTER_ADDRESSES)
        if self._initflag:
            for key in dictionary:
                self.regs.__dict__['__data_model__'][key] = dictionary[key]
        else:
            for key in dictionary:
                 self.regs.__dict__[key] = dictionary[key]
        
    
    def Save(self):
        '''Used to save the configuration of registers to a file registers.json
        '''
        dictionary = {key : self.regs.__dict__['__data_model__'][key] for key in self._cam.REGISTER_ADDRESSES}
        json_object = json.dumps(dictionary, indent='\t')
        with open('registers.json', 'w') as file:
            file.write(json_object)
    
    def Load(self):
        '''Load the values from a resgisters.json file'''
        try:
            with open('registers.json', 'r') as file:
                dictionary = json.load(file)
            for key in dictionary:
                 self.regs.__dict__['__data_model__'][key] = dictionary[key]
        except:
            warnings.warn('There is probably no load file.')
    
    async def _start_rotation(self):
        while self._running:
            with open("ions1.png", "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
            self.Image = encoded_string
            
            await asyncio.sleep(1)
            
            with open("ions2.jpg", "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
            self.Image = encoded_string
            
            await asyncio.sleep(1)

    def _startStreaming(self):
        while self._streaming:
            control_reg = self.VFB._read(0x00)

            ap_done = bool(control_reg & 0x02)
            ap_idle = bool(control_reg & 0x04)
            ap_ready = bool(control_reg & 0x08)

            write_code = lambda ctrl: (ctrl | 0x01)


            if (ap_idle and ap_ready):
                if ap_done:  
                              
                    self.Image, arr = self.Frame._getImage()
                    self.Saving._current_frame = arr
                    #print("Refreshed")

                    
                    time.sleep(0.1)
                
                self.VFB._write(0x00, write_code(control_reg))

            time.sleep(0.1)
            
    def _rotation(self):
        logging.warning('Thread process started')
        i = 0
        imax = 2**4 -1
        while self._running:
            #with open("cam_control/ions1.png", "rb") as image_file:
            #    encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
            #self.Image = encoded_string
            #
            #time.sleep(1)
            #
            #with open("cam_control/ions2.jpg", "rb") as image_file:
            #    encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
            #self.Image = encoded_string
            
            self._cam._write(self._cam.REGISTER_ADDRESSES['PGCTRL'], i<<4 | 2 | 1)
            time.sleep(0.1)
            self.Image = self.Frame._getImage()
            if i == imax:
                i = 0
            else:
                i = i + 1
            
            
        #with open("cam_control/ions1.png", "rb") as image_file:
        #    encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
        #    self.Image = encoded_string
        #self.Running = False
        logging.warning('Thread process finished')

            

    def __repr__(self):
        return "CMOS Camera Control"


if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.WARNING,
                        datefmt="%H:%M:%S")
    plugin = CamControl()
    tiqi_plugin.run(plugin,
                    host='0.0.0.0',
                    enable_rpc=True,
                    web_port=WEBPORT,
                    rpc_port=RPCPORT,
                    css=os.path.join(os.getcwd(), 'custom.css')
                    )
