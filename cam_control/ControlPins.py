from Pin import Pin, OUTPUT, INPUT, RISING, SYSFS_BASE_PATH, SYSFS_GPIO_PATH
import os 

SYSFS_EXPORT_PATH   = SYSFS_BASE_PATH + '/export'

class ControlPin:
    _value : bool = False
    def __init__(self, pin_number : int = 990, type : str = OUTPUT, **kwargs):
        if not os.path.isdir(SYSFS_GPIO_PATH % pin_number):
            with open(SYSFS_EXPORT_PATH, 'w') as export:
                export.write('%d' % pin_number)

        self._pin = Pin(pin_number, type, **kwargs)
        self._pin.set()
        self._value = bool(self._pin.read())

    @property
    def Value(self) -> bool:
        return self._value
    
    @Value.setter
    def Value(self, value):
        if value:
            self._pin.set()
        else:
            self._pin.reset()
        self._value = bool(self._pin.read())
