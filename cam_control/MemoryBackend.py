import os, mmap, struct, io, base64, logging
from PIL import Image
import numpy as np
import time
import png

class MemoryFrame:
    def __init__(self, stride = 16384, offset = 0x0f000000, shape: tuple = (720, 1280)):
        #self._f = os.open("/dev/mem", os.O_RDWR | os.O_SYNC)
        self._stride = stride
        self._offset = offset
        self._shape = shape
        self._f = os.open("/dev/udmabuf0", os.O_RDWR | os.O_SYNC)
        self._preallocateSpace()
    
    def _preallocateSpace(self):
        self._array = np.zeros(self._shape)
        self._line = np.zeros((self._shape[1]*3)//2)
        
    
    @property
    def Stride(self) -> str:
        return hex(self._stride)
    @property
    def Offset(self) -> str:
        return hex(self._offset)
    @property
    def Shape(self) -> str:
        return str(self._shape)
    
    def _getFrame(self):
        array = np.zeros(self._shape)
        nrPix = self._shape[1]
        nrBytes = int(np.ceil(nrPix*12/8))
        nrPass = nrBytes//3
        for lineNum in range(self._shape[0]):
            line = np.memmap('/dev/udmabuf0', offset = self._stride * lineNum, dtype=np.uint8, mode='r+', shape=(nrBytes))
            for i in range(nrPass):
                array[lineNum, 2*i] = line[3*i] + (line[3*i +1] & 0x0f)<<8
                array[lineNum, 2*i+1] = (line[3*i + 1]>>4) + (line[3*i +2]<<4)
            line.close()
            print(lineNum)

        return array

    #def _readFrameTryout(self):
    #    '''This one needs further analysis. I'll write it tonight.'''
    #    array = np.zeros(self._shape)
    #    nrPix = self._shape[1]
    #    nrBytes = int(np.ceil(nrPix*12/8))
    #    fmt = ''
    #    nrPass = nrBytes//3
    #    for i in range(nrBytes):
    #        fmt = fmt+'B'
    #    with mmap.mmap(self._f, self._stride*self._shape[0], mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ) as m: #offset=self._offset
    #        for lineNum in range(self._shape[0]):
    #            m.seek(lineNum*self._stride)
    #            array[lineNum,:] = self._read_uint12_var_2_prealloc(m.read(nrBytes), array[lineNum,:])
    #    return np.array(array)
                
    def _readFrameOlder(self):
        '''This one needs further analysis. I'll write it tonight.'''
        array = np.zeros(self._shape)
        nrPix = self._shape[1]
        nrBytes = int(np.ceil(nrPix*12/8))
        fmt = ''
        nrPass = nrBytes//3
        for i in range(nrBytes):
            fmt = fmt+'B'
        with mmap.mmap(self._f, self._stride*self._shape[0], mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ) as m: #offset=self._offset
            for lineNum in range(self._shape[0]):
                m.seek(lineNum*self._stride)
                line = struct.unpack(fmt, m.read(nrBytes))
                num = ''
                for i in range(nrPass):
                    #line = format(struct.unpack('B', m.read(nrBytes))[0], '#010b').split('b')[1] + num
                    array[lineNum, 2*i] = line[3*i] + (line[3*i +1] & 0x0f)<<8
                    array[lineNum, 2*i+1] = (line[3*i + 1]>>4) + (line[3*i +2]<<4)


                    #list = []
                    #lenght = len(num)
                    #for i in range(nrPix):
                    #    list.append(int('0b' + num[(lenght-12*(i+1)):(lenght-12*i)], base = 2))
                    #array.append(list)
        return np.array(array)
    
    @staticmethod
    def _read_Y12(data_chunk, prealloc):
        prealloc[:] = np.frombuffer(data_chunk, dtype=np.uint8)
        b0_uint16, b1_uint16, b2_uint16, b3_uint16, b4_uint16 = np.reshape(prealloc, (prealloc.shape[0] // 5, 5)).astype(np.uint16).T
        Y0 = b0_uint16 + (b1_uint16 & 0x000f) << 8
        Y1 = b1_uint16 >> 4 + b2_uint16 << 4
        Y2 = b3_uint16 + (b4_uint16 & 0x000f) << 8 

        return np.reshape(np.concatenate((Y0[:, None], Y1[:, None], Y2[:, None]), axis=1), 3 * Y0.shape[0])
    
    def _readFrameNewest(self):
        '''This one needs further analysis. I'll write it tonight.'''
        nrPix = self._shape[1]
        nrBytes = int(np.ceil(nrPix*5/3))
        with mmap.mmap(self._f, self._stride*self._shape[0], mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ) as m: #offset=self._offset
            for lineNum in range(self._shape[0]):
                m.seek(lineNum*self._stride)
                self._array[lineNum,:] = self._read_uint12(m.read(nrBytes), self._line)
        
    @staticmethod
    def _read_uint12(data_chunk, prealloc):
        prealloc[:] = np.frombuffer(data_chunk, dtype=np.uint8)
        fst_uint8, mid_uint8, lst_uint8 = np.reshape(prealloc, (prealloc.shape[0] // 3, 3)).astype(np.uint16).T
        #fst_uint12 = fst_uint8 | ((mid_uint8 & 0xf) << 8)
        #snd_uint12 = (mid_uint8 >> 4) | ((lst_uint8 & 0xf) << 8) | ((lst_uint8 & 0xf0) << 4)
        fst_uint12 = fst_uint8 | ((mid_uint8 & 0xf) << 8)
        snd_uint12 = (mid_uint8 >> 4) | (lst_uint8 << 4)
        return np.reshape(np.concatenate((fst_uint12[:, None], snd_uint12[:, None]), axis=1), 2 * fst_uint12.shape[0])

    #@staticmethod
    #@nb.njit(nb.uint16[::1](nb.uint8[::1],nb.uint16[::1]),fastmath=True,parallel=True,cache=True)
    #def _read_uint12_var_2_prealloc(data_chunk,out):
    #    """data_chunk is a contigous 1D array of uint8 data)
    #    eg.data_chunk = np.frombuffer(data_chunk, dtype=np.uint8)"""
#
    #    #ensure that the data_chunk has the right length
    #    assert np.mod(data_chunk.shape[0],3)==0
    #    assert out.shape[0]==data_chunk.shape[0]//3*2
#
    #    for i in nb.prange(data_chunk.shape[0]//3):
    #        fst_uint8=np.uint16(data_chunk[i*3])
    #        mid_uint8=np.uint16(data_chunk[i*3+1])
    #        lst_uint8=np.uint16(data_chunk[i*3+2])
#
    #        out[i*2] =   (fst_uint8 << 4) + (mid_uint8 >> 4)
    #        out[i*2+1] = (lst_uint8 << 4) + (15 & mid_uint8)
#
    #    return out
    def _readFrame(self):
        nrPix = self._shape[1]
        nrBytes = int(np.ceil(nrPix*12/8))

        totalReads = self._stride * self._shape[1]
        frame = np.memmap("/dev/udmabuf0", dtype = np.uint8, mode = 'r+', shape = (self._shape[0], self._stride))
        t0, t1, t2 = np.delete(np.reshape(frame, (self._shape[0], self._stride)), slice(nrBytes, self._stride), 1).reshape(1,-1).reshape((3,-1), order = 'F').astype(int)
        fst_uint12 = t0 | ((t1 & 0xf) << 8)
        snd_uint12 = (t1 >> 4) | (t2 << 4)
        return np.reshape(np.concatenate((fst_uint12[:, None], snd_uint12[:, None]), axis=1), 2 * fst_uint12.shape[0]).reshape((self._shape[0],self._shape[1]))

    def _readFrameBackup(self):
        nrPix = self._shape[1]
        nrBytes = int(np.ceil(nrPix*12/8))

        totalReads = self._stride * self._shape[1]

        with mmap.mmap(self._f, self._stride*self._shape[0], mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ) as m: #offset=self._offset
            m.seek(0)
            frame = np.frombuffer(m.read(totalReads), dtype=np.uint8)
            t0, t1, t2 = np.delete(np.reshape(frame, (self._shape[0], self._stride)), slice(nrBytes, self._stride), 1).reshape(1,-1).reshape((3,-1), order = 'F').astype(int)
            fst_uint12 = t0 | ((t1 & 0xf) << 8)
            snd_uint12 = (t1 >> 4) | (t2 << 4)
            return np.reshape(np.concatenate((fst_uint12[:, None], snd_uint12[:, None]), axis=1), 2 * fst_uint12.shape[0]).reshape((self._shape[0],self._shape[1]))



    def _readFrameOld(self):
        '''This one needs further analysis. I'll write it tonight.'''
        nrPix = self._shape[1]
        nrBytes = int(np.ceil(nrPix*12/8))
        with mmap.mmap(self._f, self._stride*self._shape[0], mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ) as m: #offset=self._offset
            for lineNum in range(self._shape[0]):
                m.seek(lineNum*self._stride)
                self._array[lineNum,:] = self._read_uint12(m.read(nrBytes), self._line)
        #return self._array
        
    def _readLine(self, lineNum):
        with mmap.mmap(self._f, self._stride, mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ, offset=self._offset + self._stride*lineNum) as m:
            #m.seek(lineNum*self._stride)
            nrPix = self._shape[1]
            num = ''
            for i in range(int(np.ceil(nrPix*12/8))):
                num = format(struct.unpack('B', m.read(1))[0], '#010b').split('b')[1] + num

            list = []
            lenght = len(num)
            for i in range(nrPix):
                list.append(int('0b' + num[(lenght-12*(i+1)):(lenght-12*i)], base = 2))
        return list

    def _getImageOld(self):
        '''Takes in an numpy array and converts it into grayscale image and encodes into base64'''
        
        #array = np.array(self._readFrame())
        #array = np.array(self._readFrameTryout())
        t = time.time()  
        array = self._readFrame()*255/1023
        t1 = time.time()
        im = Image.fromarray(array.astype('uint8'))
        #converting to base64 string
        time.sleep(0.1)
        in_mem_file = io.BytesIO()
        im.save(in_mem_file, format = "PNG")
        in_mem_file.seek(0)
        img_bytes = in_mem_file.read()
        encoded = base64.b64encode(img_bytes).decode('utf-8')
        t2 = time.time()

        print("Time elapsed during frame grabbing: {}".format(t1-t))
        print("Time elapsed during converting into html: {}".format(t2-t1))
        print("Time elapsed in total: {}".format(t2-t))
        return encoded

    def _getImage(self):
        '''Takes in an numpy array and converts it into grayscale image and encodes into base64'''
        
        #t = time.time()  
        arr = self._readFrame()
        arr1 = arr.copy()
        roi = (slice(100, -100), slice(100, -100))
        arr = arr.astype(float)
        _min, _max = arr[roi].min(), arr[roi].max()
        arr = ((arr - _min) / (_max - _min)) * 255
        arr = np.clip(arr, a_min=0, a_max=255).astype(np.uint8)

        #arr = arr.astype(float)
        #arr = (arr * 255 / np.max(arr)).astype(np.uint8)
        #arr = (self._readFrame()*255//1023).astype(np.uint8)
        #t1 = time.time()
        

        # Encode the image data as a PNG
        buffer = io.BytesIO()
        writer = png.Writer(width=arr.shape[1], height=arr.shape[0], greyscale = 'True')
        writer.write(buffer, arr)
        #t2 = time.time()
        encoded = base64.b64encode(buffer.getvalue()).decode('utf-8')
        #t3 = time.time()

        #print("----------------------------------------------")
        #print("Time elapsed during frame grabbing: {}".format(t1-t))
        #print("Time elapsed during writing into a buffer: {}".format(t2-t1))
        #print("Time elapsed during encoding: {}".format(t3-t2))
        #print("Total elapsed time: {}".format(t3-t))
        #print("----------------------------------------------")
        return encoded, arr1

    