'''The documentation for the camera which includes the description of all the registers can be found on the following link:
   https://s3.us-west-2.amazonaws.com/secure.notion-static.com/c316df94-2967-458a-85ca-04c605cef0d6/sony_imx290lqr-c_datasheet.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20221028%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20221028T131408Z&X-Amz-Expires=86400&X-Amz-Signature=85f209fe556fac6abd17eec3881717296df2d90036009e8e6e938fb0f52d8089&X-Amz-SignedHeaders=host&response-content-disposition=filename%3D%22sony_imx290lqr-c_datasheet.pdf%22&x-id=GetObject
'''
from base import CameraBase, RegisterBase
from intitialConfig import initial, settings, initial2, settings2
import time

class Camera(CameraBase):
    '''Base class for CMOS camera used for fast state detection measurement. The model of the camera is'''
    
    def __init__(self, i2c_address : int = 0x36, device: int = 0):
        super().__init__(i2c_address, device)
        self.REGISTER_ADDRESSES = REGISTER_ADDRESSES
        self.REGISTERS = REGISTERS()
        #self._initRegs = INIT_REGS
        
        self._initRegs = initial2
        self._defaultSettings = settings2

        self.reset()

    def writeDefault(self):
        for reg in self._defaultSettings:
            self._write(int(reg, base=16), self._defaultSettings[reg])

    def start(self):
        self._write(self.REGISTER_ADDRESSES['STANDBY'], 0)
        time.sleep(0.1)
        self._write(self.REGISTER_ADDRESSES['XMSTA'], 0)
    
    def stop(self):
        self._write(self.REGISTER_ADDRESSES['XMSTA'], 1)
        time.sleep(0.1)
        self._write(self.REGISTER_ADDRESSES['STANDBY'], 1)
    
    def reset(self):
        for reg in self._initRegs:
            self._write(int(reg, base=16), self._initRegs[reg])
    
    
REGISTER_ADDRESSES = {
    'STANDBY' : 0x3000,
    'REGHOLD' : 0x3001,
    'XMSTA' : 0x3002,
    'SW_RESET' : 0x3003,
    'ADBIT' : 0x3005,
    'VREVERSE_HREVERSE_WINMODE' : 0x3007,
    'FRSEL_FDG_SEL' : 0x3009,
    'BLKLEVEL_LSB' : 0x300a,
    'BLKLEVEL_MSB' : 0x300b,
    'GAIN' : 0x3014,
    'VMAX_MSB' : 0x301a,
    'VMAX_SB' : 0x3019,
    'VMAX_LSB' : 0x3018,
    'HMAX_MSB' : 0x301d,
    'HMAX_LSB' : 0x301c,
    'SHS1_MSB' : 0x3022,
    'SHS1_SB' : 0x3021,
    'SHS1_LSB' : 0x3020,
    'WINWV_OB' : 0x303a,
    'WINPV_MSB' : 0x303d,
    'WINPV_LSB' : 0x303c,
    'WINWV_MSB' : 0x303f,
    'WINWV_LSB' : 0x303e,
    'WINPH_MSB' : 0x3041,
    'WINPH_LSB' : 0x3040,
    'WINWH_MSB' : 0x3043,
    'WINWH_LSB' : 0x3042,
    'OPORTSEL_ODBIT' : 0x3046,
    'XVSLNG' : 0x3048,
    'XHSLNG' : 0x3049,
    'XHSOUTSEL_XVSOUTSEL' : 0x304b,
    'INCKSEL1' : 0x305c,
    'INCKSEL2' : 0x305d,
    'INCKSEL3' : 0x305e,
    'INCKSEL4' : 0x305f,
    'INCKSEL5' : 0x315e,
    'INCKSEL6' : 0x3164,
    'ADBIT1' : 0x3129,
    'ADBIT2' : 0x317c,
    'ADBIT3' : 0x31ec,
    'REPETITION' : 0x3405,
    'PHYSICAL_LANE_NUM' : 0x3407,
    'OPB_SIZE_V' : 0x3414,
    'Y_OUT_SIZE_MSB' : 0x3419,
    'Y_OUT_SIZE_LSB' : 0x3418,
    'CSI_DT_FMT_MSB' : 0x3442,
    'CSI_DT_FMT_LSB' : 0x3441,
    'CSI_LANE_MODE' : 0x3443,
    'EXTCK_FREQ_MSB' : 0x3445,
    'EXTCK_FREQ_LSB' : 0x3444,
    'TCLKPOST_MSB' : 0x3447,
    'TCLKPOST_LSB' : 0x3446,
    'THSZERO_MSB' : 0x3449,
    'THSZERO_LSB' : 0x3448,
    'THSEXIT_MSB' : 0x342D,
    'THSEXIT_LSB' : 0x342C,
    'THSPREPARE_MSB' : 0x344b,
    'THSPREPARE_LSB' : 0x344a,
    'TCLKTRAIL_MSB' : 0x344d,
    'TCLKTRAIL_LSB' : 0x344c,
    'TCLKPRE_MSB' : 0x3431,
    'TCLKPRE_LSB' : 0x3430,
    'THSTRAIL_MSB' : 0x344f,
    'THSTRAIL_LSB' : 0x344e,
    'TCLKZERO_MSB' : 0x3451,
    'TCLKZERO_LSB' : 0x3450,
    'TCLKPREPARE_MSB' : 0x3453,
    'TCLKPREPARE_LSB' : 0x3452,
    'TLPX_MSB' : 0x3455,
    'TLPX_LSB' : 0x3454,
    'X_OUT_SIZE_MSB' : 0x3473,
    'X_OUT_SIZE_LSB' : 0x3472,
    'PGCTRL' : 0x308c,
}

class REGISTERS(RegisterBase):
    STANDBY: int
    REGHOLD: int
    XMSTA : int
    ADBIT : int
    VREVERSE_HREVERS_WINMODE : int
    FRSEL_FDG_SEL : int
    BLKLEVEL_LSB : int 
    BLKLEVEL_MSB : int 
    GAIN : int
    VMAX_MSB : int
    VMAX_SB : int
    VMAX_LSB : int
    HMAX_MSB : int
    HMAX_LSB : int
    SHS1_MSB : int
    SHS1_SB : int
    SHS1_LSB : int
    WINWV_OB : int
    WINPV_MSB : int
    WINPV_LSB : int
    WINWV_MSB : int
    WINWV_LSB : int
    WINPH_MSB : int
    WINPH_LSB : int
    WINWH_MSB : int
    WINWH_LSB : int
    OPORTSEL_ODBIT : int
    XVSLNG : int
    XHSLNG : int
    XHSOUTSEL_XVSOUTSEL : int
    INCKSEL1 : int
    INCKSEL2 : int
    INCKSEL3 : int
    INCKSEL4 : int
    INCKSEL5 : int
    INCKSEL6 : int
    ADBIT1 : int
    ADBIT2 : int 
    ADBIT3 : int
    REPETITION : int
    PHYSICAL_LANE_NUM : int
    OPB_SIZE_V : int
    Y_OUT_SIZE_MSB : int
    Y_OUT_SIZE_LSB : int
    CSI_DT_FMT_MSB : int
    CSI_DT_FMT_LSB : int
    CSI_LANE_MODE : int
    EXTCK_FREQ_MSB : int
    EXTCK_FREQ_LSB : int
    TCLKPOST_MSB : int
    TCLKPOST_LSB : int
    THSZERO_MSB : int
    THSZERO_LSB : int
    THSEXIT_MSB : int
    THSEXIT_LSB : int
    THSPREPARE_MSB : int
    THSPREPARE_LSB : int
    TCLKTRAIL_MSB : int 
    TCLKTRAIL_LSB : int
    TCLKPRE_MSB : int 
    TCLKPRE_LSB : int
    THSTRAIL_MSB : int
    THSTRAIL_LSB : int
    TCLKZERO_MSB : int
    TCLKZERO_LSB : int
    TCLKPREPARE_MSB : int
    TCLKPREPARE_LSB : int
    TLPX_MSB : int
    TLPX_LSB : int
    X_OUT_SIZE_MSB : int
    X_OUT_SIZE_LSB : int
    PGCTRL : int
    SW_RESET : int
    
    _metadata = {
        'STANDBY' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REGHOLD' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'XMSTA' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'SW_RESET' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'ADBIT' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'VREVERSE_HREVERSE_WINMODE' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'FRSEL_FDG_SEL' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'BLKLEVEL_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'BLKLEVEL_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'GAIN' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'VMAX_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'VMAX_SB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'VMAX_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'HMAX_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'HMAX_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'SHS1_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'SHS1_SB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'SHS1_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINWV_OB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINPV_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINPV_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINWV_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINWV_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINPH_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINPH_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINWH_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'WINWH_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'OPORTSEL_ODBIT' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'XVSLNG' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'XHSLNG' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'XHSOUTSEL_XVSOUTSEL' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'INCKSEL1' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'INCKSEL2' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'INCKSEL3' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'INCKSEL4' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'INCKSEL5' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'INCKSEL6' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'ADBIT1' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'ADBIT2' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'ADBIT3' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'REPETITION' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'PHYSICAL_LANE_NUM' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'OPB_SIZE_V' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'Y_OUT_SIZE_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'Y_OUT_SIZE_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'CSI_DT_FMT_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'CSI_DT_FMT_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'CSI_LANE_MODE' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'EXTCK_FREQ_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'EXTCK_FREQ_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKPOST_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKPOST_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSZERO_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSZERO_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSEXIT_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSEXIT_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSPREPARE_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSPREPARE_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKTRAIL_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKTRAIL_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKPRE_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKPRE_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSTRAIL_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'THSTRAIL_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKZERO_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKZERO_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKPREPARE_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TCLKPREPARE_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TLPX_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'TLPX_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'X_OUT_SIZE_MSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'X_OUT_SIZE_LSB' : {'min' : 0, 'max' : 0xff, 'step' : 1},
        'PGCTRL' : {'min' : 0, 'max' : 0xff, 'step' : 1},
    }


INIT_REGS = {
    '0x3018' : 0x65,
    '0x3019' : 0x04,
    '0x301a' : 0x00,
    '0x3444' : 0x20,
    '0x3445' : 0x25,
    '0x3007' : 0x00,
    '0x301A' : 0x00,
    '0x303A' : 0x0C,
    '0x3040' : 0x00,
    '0x3041' : 0X00,
    '0x303c' : 0x00,
    '0x303d' : 0x00,
    '0x3042' : 0x9C,
    '0x3043' : 0x07,
    '0x303e' : 0x49,
    '0x303f' : 0x04,
    '0x304b' : 0x0A,
    '0x300F' : 0x00,
    '0x3010' : 0x21,
    '0x3012' : 0x64,
    '0x3013' : 0x00,
    '0x3016' : 0x09,
    '0x3070' : 0x02,
    '0x3071' : 0x11,
    '0x309B' : 0x10,
    '0x309C' : 0x22,
    '0x30A2' : 0x02,
    '0x30A6' : 0x20,
    '0x30A8' : 0x20,
    '0x30AA' : 0x20,
    '0x30AC' : 0x20,
    '0x30B0' : 0x43,
    '0x3119' : 0x9E,
    '0x311C' : 0x1E,
    '0x311E' : 0x08,
    '0x3128' : 0x05,
    '0x313D' : 0x83,
    '0x3150' : 0x03,
    '0x317E' : 0x00,
    '0x32B8' : 0x50,
    '0x32B9' : 0x10,
    '0x32BA' : 0x00,
    '0x32BB' : 0x04,
    '0x32C8' : 0x50,
    '0x32C9' : 0x10,
    '0x32CA' : 0x00,
    '0x32CB' : 0x04,
    '0x332C' : 0xD3,
    '0x332D' : 0x10,
    '0x332E' : 0x0D,
    '0x3358' : 0x06,
    '0x3359' : 0xE1,
    '0x335A' : 0x11,
    '0x3360' : 0x1E,
    '0x3361' : 0x61,
    '0x3362' : 0x10,
    '0x33B0' : 0x50,
    '0x33B2' : 0x1A,
    '0x33B3' : 0x04,
    '0x3480' : 0x49
}