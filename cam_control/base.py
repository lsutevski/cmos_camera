from smbus import SMBus
import os, mmap, struct
from abc import ABC, abstractmethod
import warnings

class CameraBase(ABC):
    '''Base class for camera used for state detection measurement.
       Assumption is that the commmunication with the camera is based on
       i2c. Also I assume the registers are 16-bit which I think is a standard
       thing more or less.
    '''

    def __init__(self, i2c_address, device):
        self.addr = i2c_address
        self.bus = SMBus(device)
        self.REGISTER_ADDRESSES = {}
        self.REGISTERS = {}
        self.RandomRegister = RandomRegister()
        
    @abstractmethod
    def start(self):
        pass
    
    @abstractmethod
    def stop(self):
        pass
    
    
    def _read(self, reg: int):
        '''This method reads from the register of the camera specified by reg.
        '''
        try:
            reg_MSB = reg>>8
            reg_LSB = reg & 0xff
            self.bus.write_byte_data(self.addr, reg_MSB, reg_LSB)
            return self.bus.read_byte(self.addr)
        except:
            warnings.warn('I guess something is not connected.')
    
    def _write(self, reg : int, val : int):
        '''This method writes into the register of the camera specified by reg.
           The value should be one byte.
        '''
        try:
            reg_MSB = reg>>8
            reg_LSB = reg & 0xff
            self.bus.write_i2c_block_data(self.addr, reg_MSB, [reg_LSB, val])
        except:
            warnings.warn('I guess something is not connected.')
        
    def configure(self, **kwargs):
        '''This method should be used in order to set multiple register at once.
           Usege should be like following:
                camera.configure(reg1 = val1, reg2 = val2})
        '''
        for reg in kwargs:
            self._write(self.REGISTER_ADDRESSES[reg], kwargs[reg])

    def read_config(self, regs):
        '''Read the values of the registers specified by list. The method returns
           a dictionary of the form: {<name from regs> : <value from regs>}.
        '''
        output = {}
        for reg in regs:
            output[reg] = self._read(regs[reg])
        return output

class RegisterBase:
    _metadata = {}

class RandomRegister:
    _random_register = 0
    _value = 0
    _metadata = {
        'Value' : {'min' : 0, 'max' : 0xff, 'step' : 1}
    }
    @property
    def Random_register(self):
        return self._random_register
    
    @Random_register.setter
    def Random_register(self, value):
        self._random_register = value
    
    @property
    def Value(self):
        return self._value
    
    @Value.setter
    def Value(self, value):
        self._value = value

class MemoryDevice:
    def __init__(self, base_addr : int):
        self._offset = base_addr
    
    def _read(self, addr: int):
        f = os.open("/dev/mem", os.O_RDWR | os.O_SYNC)
        with mmap.mmap(f, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ, offset=self._offset) as m:
            m.seek(addr)
            val = struct.unpack('I', m.read(4))[0]
        os.close(f)
        return val

    def _write(self, addr: int, val: int):
        f = os.open("/dev/mem", os.O_RDWR | os.O_SYNC)
        with mmap.mmap(f, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_WRITE | mmap.PROT_READ, offset=self._offset) as m:
            m.seek(addr)
            m.write(struct.pack('I', val))
        os.close(f)
