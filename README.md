# CMOS_camera_control

A plugin to control the CMOS camera. It is meant to be run on ZedBoard in combination with the embedded linux created with Buildroot ([sony-cmos-camera](https://gitlab.phys.ethz.ch/tiqi-projects/fpga/sony-cmos-camera)). In order for this code to work one needs to follow the instructions on the linked readme and configure the git.
<br>
After that is done, simply create a new folder where you want to place the code and use the following command:<br>
```
git clone https://gitlab.phys.ethz.ch/tiqi-projects/fpga/sony-cmos-camera.git
```
Once that is done, the code can be run only if the sony sensor is connected (this needs to be modified for easier life). In order to run the code, position yourself inside the *cam_control* directory and run:<br>
```
python main.py
```
All the dependancies should be included in the embedded linux. If something is not there, I either forgot to fix it or I imported it manually during the development. In that case take a look at the errors that the code returns and feel free to include them in the buildroot :).
<br>
Detailed desctiption of the code can be found in this [thesis](https://ethz.ch/content/dam/ethz/special-interest/phys/quantum-electronics/tiqi-dam/documents/masters_theses/MasterThesis_LeoSutevski). The UML diagrm of the software can be seen in the following file [file](Diagram.pdf).


## Requirements

 - Python 3.6+
 - [poetry](https://python-poetry.org/)


## Makefile targets:

- `prepare`: install the required dependencies using poetry. On Windows, run `poetry install`.
- `run`: run the plugin. On Windows, `poetry run python -m cam_control.main`.
- `clean`: removes poetry lockfile and virtual environment.
- `services` (Unix only): create and install a unit service file to runs the plugin server via `systemctl`.
